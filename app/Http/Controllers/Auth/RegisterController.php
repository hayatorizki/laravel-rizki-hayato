<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'name' => ['required'],
            'email' => ['required'],
            'password' => ['required'],
            'aud' => ['required'],
            'sub' => ['required'],
            'company_id' => ['required'],
            'iat' => ['required'],
            'exp' => ['required'],
            'iss' => ['required'],
        ]);
        User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'aud' => request('aud'),
            'sub' => request('sub'),
            'company_id' => request('company_id'),
            'iat' => request('iat'),
            'exp' => request('exp'),
            'iss' => request('iss'),
        ]);
        return response('You are registered');
    }
}
